from setuptools import setup, find_packages


setup(
	name = 'myuzi',
	version = '0.9.1',
	author = 'zehkira',
	description = 'Modules for Myuzi',
	url = 'https://gitlab.com/zehkira/myuzi',
	packages = find_packages(),
	install_requires = ['requests', 'beautifulsoup4'],
	classifiers = [
		'Programming Language :: Python :: 3.6',
		'Operating System :: POSIX :: Linux'
	],
	python_requires = '>=3.6',
)
