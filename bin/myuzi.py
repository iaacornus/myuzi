import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, Gst

from myuzi import app


### --- MAIN --- ###


def main() -> None:
	# gst needs to be initialized first
	Gst.init([])

	# make a playbin
	app.Player.playbin = Gst.ElementFactory.make('playbin', 'playbin')
	app.Player.playbin.set_state(Gst.State.READY)

	# run the app
	gtkApp = Gtk.Application()
	gtkApp.connect('activate', app.build)
	gtkApp.run()


if __name__ == '__main__':
	main()

