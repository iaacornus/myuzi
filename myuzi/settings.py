from json import loads, dumps
from os import makedirs
from pathlib import Path


# settings = {
# 	'volume': '0.5'
# }


def set_value(key: str, value) -> None:
	settings = read_settings()
	settings[key] = str(value)
	write_settings(settings)


def get_value(key: str, default: str = '') -> str:
	return read_settings().get(key, default)


def write_settings(settings: dict) -> None:
	dir_path = Path.home() / Path('.config/myuzi/')
	sets_path = dir_path / Path('settings.json')

	try:
		with open(str(sets_path), 'w', encoding='utf-8') as sets_file:
			sets_file.write(dumps(settings))
	except FileNotFoundError:
		makedirs(str(dir_path))
		write_settings(settings)


def read_settings() -> dict:
	sets_path = str(
		Path.home() / Path('.config/myuzi/settings.json')
	)

	try:
		with open(sets_path, 'r', encoding='utf-8') as sets_file:
			return loads(sets_file.read())
	except OSError:
		return {}

