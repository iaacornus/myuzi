from glob import glob
from pathlib import Path
from subprocess import Popen, PIPE
from os import makedirs, rename, remove
from os.path import exists

from myuzi import playlists


def is_song_cached(video_id: str) -> bool:
	return exists(
		str(
            Path.home() / Path(
                '.config/myuzi/cache/' + video_id
            )
        )
	)


# saves song to local storage
def cache_song(video_id: str) -> None:
	# don't overwrite. yt video content doesn't change
	if is_song_cached(video_id):
		return

	path = str(Path.home() / Path('.config/myuzi/cache/'))
	makedirs(path, exist_ok=True)

	# --no-cache-dir might help with 403 errors
	out, _ = Popen(
		(
            f'yt-dlp -x --no-cache-dir --audio-quality 0 -o "{path}/%'
            f'(id)s.%(ext)s" https://www.youtube.com/watch?v={video_id}'
        ),
		shell=True,
		stdout=PIPE
	).communicate()

	# rename id.* files to id
	for file in glob(path + '/*.*'):
		rename(file, '.'.join(file.split('.')[:-1]))


# removes song from local storage
def uncache_song(video_id: str) -> None:
	try:
		remove(
			str(
                Path.home() / Path(
                    '.config/myuzi/cache/' + video_id
                )
            )
		)
	except FileNotFoundError:
		pass


def cache_playlist(name: str) -> None:
	for song in playlists.read_playlists()[name]:
		cache_song(song['id'])


def uncache_playlist(name: str) -> None:
	for song in playlists.read_playlists()[name]:
		uncache_song(song['id'])
