import logging
from time import sleep
from random import shuffle

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, GLib, Gst

from myuzi import cache, playlists, utils, settings


### --- DATA CLASSES --- ###
class Interface:
	window: Gtk.ApplicationWindow
	box_results: Gtk.Box
	box_playlists: Gtk.Box
	scr_settings: Gtk.ScrolledWindow
	lbl_title: Gtk.Label
	bar_progress: Gtk.ProgressBar
	btn_pause: Gtk.Button
	tog_loop: Gtk.ToggleButton
	collapsed_playlists: list = [name for name in playlists.read_playlists()]


class Player:
	playbin: Gst.Element
	playbin_lock = GLib.Mutex()
	index: int = 0
	queue: list = []
	search_results: list = []
	playback_retries: int = 0
	online: bool = utils.is_yt_available()


### --- MISC --- ###

# handle async song playback and sync interface update
def play_song(id_: str, title: str, author: str) -> None:
	logging.info(f'Playing {title} by {author}, id {id_}')

	# update player interface
	Interface.btn_pause.set_label('❙❙')
	Interface.lbl_title.set_text(title)

	def playback_thread(id_: str, title: str, author: str) -> None:
		# reset playbin and prep for playback
		Player.playbin_lock.lock()
		remake_playbin()
		Player.playbin.set_state(Gst.State.READY)
		Player.playbin.set_property('uri', utils.get_song_uri(id_))

		# set up bus
		bus = Player.playbin.get_bus()
		bus.add_signal_watch()
		bus.connect('message::error', _on_bus_error, id_, title, author)
		bus.connect('message::eos', _on_song_end)

		# play stream
		Player.playbin.set_state(Gst.State.PLAYING)
		Player.playbin_lock.unlock()

	if Player.online or cache.is_song_cached(id_):
		GLib.Thread.new(None, playback_thread, id_, title, author)
	else:
		logging.info(f'Skipping {title} by {author}, id {id_} - offline mode')
		_on_next_song(None)


# moves progress bar, call every second or so
def update_progress() -> bool:
	Player.playbin_lock.lock()
	duration = Player.playbin.query_duration(Gst.Format.TIME)[1]
	position = Player.playbin.query_position(Gst.Format.TIME)[1]

	if duration > 0:
		Interface.bar_progress.set_fraction(position / duration)
	else:
		Interface.bar_progress.set_fraction(0)

	Player.playbin_lock.unlock()
	return True


def remake_playbin() -> None:
	# clean delete
	Player.playbin.set_state(Gst.State.NULL)
	del Player.playbin

	# new playbin
	Player.playbin = Gst.ElementFactory.make('playbin', 'playbin')
	Player.playbin.set_state(Gst.State.READY)
	Player.playbin.set_property('volume', float(settings.get_value('volume', 1)))
	Player.playbin.set_property('mute', False)


def play_radio_song() -> bool | None:
	if not Player.online or not int(settings.get_value('radio')):
		return

	# show status
	Interface.lbl_status.set_text('Searching for another song to play...')
	Interface.window.set_sensitive(False)

	id_queue = [s['id'] for s in Player.queue]
	shuffle(id_queue)

	# thread to find and play a song
	def radio_search() -> None:
		Player.playbin_lock.lock()
		for id_ in id_queue:
			song = utils.get_similar_song(id_, ignore=id_queue)
			if song:
				logging.info('Playing recommended song')
				Player.queue.append(song)
				Player.index += 1
				Player.playback_retries = 0
				play_song(song['id'], song['title'], song['author'])
				break

		Player.playbin_lock.unlock()

	# timeout to unlock interface when done
	def post_radio_search_check() -> bool:
		# i wanted to pulse() here but it just refused to work
		if Player.playbin_lock.trylock():
			Player.playbin_lock.unlock()

			Interface.lbl_status.set_text('')
			Interface.window.set_sensitive(True)
			return False

		return True

	GLib.timeout_add_seconds(1, post_radio_search_check)
	GLib.Thread.new('radio search thread', radio_search)


### --- EVENT FUNCTIONS --- ###


# stream error event (usually 403)
def _on_bus_error(
        _, err, song_id: str, song_title: str, song_author: str
    ) -> None:
	err = err.parse_error()
	logging.error(f'Bus error: {err}')

	# delete everything, retry from scratch
	remake_playbin()

	# wait before retrying - if it's 403 yt might block again
	sleep(2)

	if Player.playback_retries > 1:
		# inform user of error
		logging.error('Failed to recover from bus error')
		msg_dialog = Gtk.MessageDialog(
			parent=Interface.window,
			destroy_with_parent=True,
			modal=True,
			buttons=Gtk.ButtonsType.OK,
			text='Could not play song',
			secondary_text=f'<{song_id}>: {err.gerror}'
		)
		msg_dialog.run()
		msg_dialog.destroy()

		# clean up
		Interface.bar_progress.set_fraction(0)
		Interface.lbl_title.set_text('...')
		Interface.btn_pause.set_label('▶')
		Player.playback_retries = 0
		return

	logging.warn('Retrying playback')
	Player.playback_retries += 1
	play_song(song_id, song_title, song_author)


# stream end event, play next in queue
def _on_song_end(*_) -> None:
	# avoid receiving multiple eos signals
	if Player.playbin.get_bus().have_pending():
		return

	# all is well
	Player.playback_retries = 0

	# get rid of playbin, make a new one
	remake_playbin()

	# reset interface
	Interface.bar_progress.set_fraction(0)
	Interface.lbl_title.set_text('...')

	# loop, queue
	song = None
	if Interface.tog_loop.get_active():
		song = Player.queue[Player.index]
	if len(Player.queue) - 1 > Player.index:
		Player.index += 1
		song = Player.queue[Player.index]
	if song is not None:
		play_song(song['id'], song['title'], song['author'])
		return

	play_radio_song()


# play/pause button click event
def _on_toggle_pause(_) -> None:
	# inf timeout, second element of returned thing is the actual state
	state = Player.playbin.get_state(Gst.CLOCK_TIME_NONE)[1]

	if state == Gst.State.PLAYING:
		Player.playbin.set_state(Gst.State.PAUSED)
		Interface.btn_pause.set_label('▶')
	else:
		Player.playbin.set_state(Gst.State.PLAYING)
		Interface.btn_pause.set_label('❙❙')


# next song button click event
def _on_next_song(_) -> None:
	# all is well
	Player.playback_retries = 0

	# get rid of playbin, make a new one
	remake_playbin()

	# reset interface
	Interface.bar_progress.set_fraction(0)
	Interface.lbl_title.set_text('...')

	# next in queue
	if len(Player.queue) - 1 > Player.index :
		Player.index += 1
		Player.playback_retries = 0
		song = Player.queue[Player.index]
		play_song(song['id'], song['title'], song['author'])
		return

	# find and play new song
	play_radio_song()


# previous song button click event
def _on_previous_song(_) -> None:
	Player.playbin.set_state(Gst.State.READY)
	Player.index -= 1
	Player.playback_retries = 0

	# will restart song if nothing before it in queue
	if Player.index < 0:
		Player.index = 0

	# play the thing if queue not empty
	if len(Player.queue) > 0:
		song = Player.queue[Player.index]
		play_song(song['id'], song['title'], song['author'])


# play queue starting at index
def _on_play_queue(_, queue: list, index: int) -> None:
	logging.info('New queue')

	if not Player.online:
		queue = [song for song in queue if cache.is_song_cached(song['id'])]

	if len(queue) == 0:
		logging.info('Not playing, empty queue')
		return

	Player.queue = queue
	Player.index = index
	Player.playback_retries = 0
	song = queue[index]
	play_song(song['id'], song['title'], song['author'])


# swap songs in playlist event, from playlists tab
def _on_swap_in_playlist(_, i: int, j: int, playlist: str) -> None:
	playlists.swap_songs(i, j, playlist)
	build_playlists()


# add song to playlist event, from search tab
def _on_add_to_playlist(
        _, id_: str, title: str, author: str, playlist: str
    ) -> None:
	playlists.add_song(id_, title, author, playlist)
	# don't rebuild playlists, because we're in the search tab anyway


# rename song in playlist event, from playlists tab
def _on_rename_song(_, index: int, name: str, playlist: str) -> None:
	dialog = Gtk.Dialog.new()
	dialog.set_title('Rename song')
	dialog.set_transient_for(Interface.window)
	dialog.add_buttons(
        Gtk.STOCK_CANCEL,
        Gtk.ResponseType.CANCEL,
        Gtk.STOCK_OK,
        Gtk.ResponseType.OK
    )
	dialog.set_resizable(False)

	ent_name = Gtk.Entry.new()
	ent_name.set_halign(Gtk.Align.FILL)
	ent_name.set_text(name)

	box = dialog.get_content_area()
	box.add(ent_name)
	dialog.show_all()

	if dialog.run() == Gtk.ResponseType.OK:
		playlists.rename_song(index, playlist, ent_name.get_text())
		build_playlists()

	dialog.destroy()


# save song to local storage event, from playlists tab
def _on_cache_song(_, id_: str) -> bool:
	Interface.window.set_sensitive(False)
	Interface.lbl_status.set_text('Downloading song...')
	Interface.bar_status.set_visible(True)
	Interface.bar_status.set_fraction(0)

	def post_cache_unlock(id_: str):
		Interface.bar_status.pulse()

		if cache.is_song_cached(id_):
			Interface.bar_status.set_visible(False)
			Interface.lbl_status.set_text('')
			Interface.window.set_sensitive(True)
			build_playlists()
			return False

		return True

	GLib.Thread.new('cache thread', cache.cache_song, id_)
	GLib.timeout_add_seconds(1, post_cache_unlock, id_)


# remove song from local storage event, from playlists tab
def _on_uncache_song(_, id_: str) -> None:
	cache.uncache_song(id_)
	build_playlists()


# remove song from playlist event, from playlists tab
def _on_remove_from_playlist(_, index: int, playlist: str) -> None:
	cache.uncache_song(playlists.read_playlists()[playlist][index]['id'])
	playlists.remove_song(index, playlist)
	build_playlists()


# create playlist event, from playlists tab
def _on_create_playlist(_, entry: Gtk.Entry) -> None:
	name = entry.get_text().strip()
	if name == '':
		msg_dialog = Gtk.MessageDialog(
			parent=Interface.window,
			destroy_with_parent=True,
			modal=True,
			buttons=Gtk.ButtonsType.OK,
			text='Could not create playlist',
			secondary_text='Enter a name first.'
		)
		msg_dialog.run()
		msg_dialog.destroy()
		return

	if playlists.add_playlist(name):
		build_playlists()
	else:
		msg_dialog = Gtk.MessageDialog(
			parent=Interface.window,
			destroy_with_parent=True,
			modal=True,
			buttons=Gtk.ButtonsType.OK,
			text='Could not create playlist',
			secondary_text='A playlist with that name already exists.'
		)
		msg_dialog.run()
		msg_dialog.destroy()


# import playlist event, from playlists tab
def _on_import_playlist(_, entry: Gtk.Entry) -> None:
	name = entry.get_text().strip()
	if name == '':
		msg_dialog = Gtk.MessageDialog(
			parent=Interface.window,
			destroy_with_parent=True,
			modal=True,
			buttons=Gtk.ButtonsType.OK,
			text='Could not import playlist',
			secondary_text='Enter a name first.'
		)
		msg_dialog.run()
		msg_dialog.destroy()
		return

	dialog = Gtk.Dialog.new()
	dialog.set_title('Import playlist')
	dialog.set_transient_for(Interface.window)
	dialog.add_buttons(
        Gtk.STOCK_CANCEL,
        Gtk.ResponseType.CANCEL,
        Gtk.STOCK_OK,
        Gtk.ResponseType.OK
    )
	dialog.set_resizable(False)

	ent_data = Gtk.Entry.new()
	ent_data.set_halign(Gtk.Align.FILL)

	lbl_help = Gtk.Label.new('Paste playlist data below.')

	box = dialog.get_content_area()
	box.add(lbl_help)
	box.add(ent_data)
	dialog.show_all()

	if dialog.run() == Gtk.ResponseType.OK:
		success, msg = playlists.import_playlist(name, ent_data.get_text())
		if success:
			build_playlists()
		else:
			msg_dialog = Gtk.MessageDialog(
				parent=Interface.window,
				destroy_with_parent=True,
				modal=True,
				buttons=Gtk.ButtonsType.OK,
				text='Could not import playlist',
				secondary_text=msg
			)
			msg_dialog.run()
			msg_dialog.destroy()

	dialog.destroy()


# rename playlist event, from playlists tab
def _on_rename_playlist(_, name: str) -> None:
	dialog = Gtk.Dialog.new()
	dialog.set_title('Rename playlist')
	dialog.set_transient_for(Interface.window)
	dialog.add_buttons(
        Gtk.STOCK_CANCEL,
        Gtk.ResponseType.CANCEL,
        Gtk.STOCK_OK,
        Gtk.ResponseType.OK
    )
	dialog.set_resizable(False)

	ent_name = Gtk.Entry.new()
	ent_name.set_halign(Gtk.Align.FILL)
	ent_name.set_text(name)

	box = dialog.get_content_area()
	box.add(ent_name)
	dialog.show_all()

	if dialog.run() == Gtk.ResponseType.OK:
		if playlists.rename_playlist(name, ent_name.get_text()):
			build_playlists()
		else:
			msg_dialog = Gtk.MessageDialog(
				parent=Interface.window,
				destroy_with_parent=True,
				modal=True,
				buttons=Gtk.ButtonsType.OK,
				text='Could not rename playlist',
				secondary_text='A playlist with that name already exists.'
			)
			msg_dialog.run()
			msg_dialog.destroy()

	dialog.destroy()


# share playlist event, from playlists tab
def _on_share_playlist(_, name: str) -> None:
	utils.copy_to_clipboard(playlists.export_playlist(name))
	msg_dialog = Gtk.MessageDialog(
		parent=Interface.window,
		destroy_with_parent=True,
		modal=True,
		buttons=Gtk.ButtonsType.OK,
		secondary_text='Playlist data copied to clipboard.'
	)
	msg_dialog.run()
	msg_dialog.destroy()


# expand playlist event, from playlists tab
def _on_expand_playlist(button: Gtk.Button, box: Gtk.Box, name: str) -> None:
	visible = box.get_visible()
	box.set_visible(not visible)
	button.set_label('▶' if visible else '▼')
	if visible:
		if name not in Interface.collapsed_playlists:
			Interface.collapsed_playlists.append(name)
	elif name in Interface.collapsed_playlists:
		Interface.collapsed_playlists.remove(name)


def _on_cache_playlist(_, name: str) -> None:
	Interface.window.set_sensitive(False)
	Interface.lbl_status.set_text('Downloading playlist...')
	Interface.bar_status.set_visible(True)
	Interface.bar_status.set_fraction(0)
	songs = playlists.read_playlists()[name]

	def cache_progress(songs: list):
		# calculate progress
		cached = 0
		for song in songs:
			if cache.is_song_cached(song['id']):
				cached += 1

		# update progress bar
		progress = cached / len(songs)
		Interface.bar_status.set_fraction(progress)

		# done, clean up
		if progress == 1:
			Interface.lbl_status.set_text('')
			Interface.bar_status.set_visible(False)
			build_playlists()
			Interface.window.set_sensitive(True)
			return False

		return True

	GLib.Thread.new('cache thread', cache.cache_playlist, name)
	GLib.timeout_add_seconds(1, cache_progress, songs)


def _on_uncache_playlist(_, name: str) -> None:
	cache.uncache_playlist(name)
	build_playlists()


# delete playlist event, from playlists tab
def _on_delete_playlist(_, name: str) -> None:
	cache.uncache_playlist(name)
	playlists.remove_playlist(name)
	build_playlists()


# search event, from search tab
def _on_search(entry) -> None:
	if not Player.online:
		msg_dialog = Gtk.MessageDialog(
			parent=Interface.window,
			destroy_with_parent=True,
			modal=True,
			buttons=Gtk.ButtonsType.OK,
			text='Unavailable',
			secondary_text='Can\'t search in offline mode.'
		)
		msg_dialog.run()
		msg_dialog.destroy()
		return

	Player.search_results = utils.find_songs(entry.get_text())
	build_search()


def _on_toggle_online(toggle: Gtk.ToggleButton) -> None:
	Player.online = False
	toggle.set_label('Offline')

	if toggle.get_active():
		if utils.is_yt_available():
			Player.online = True
			toggle.set_label('Online')
		else:
			toggle.set_active(False)
			msg_dialog = Gtk.MessageDialog(
				parent=Interface.window,
				destroy_with_parent=True,
				modal=True,
				buttons=Gtk.ButtonsType.OK,
				text='Can\'t enter online mode',
				secondary_text='Connection to YT failed.'
			)
			msg_dialog.run()
			msg_dialog.destroy()

	build_search()
	build_playlists()


# switch tab event
def _on_switch_page(_, page, page_num) -> None:
	if page_num == 0:
		build_playlists()
	elif page_num == 1:
		build_search()


# volume change event, from settings tab
def _on_volume_change(scale: Gtk.Scale) -> None:
	volume = scale.get_value()
	Player.playbin.set_property('volume', volume)
	settings.set_value('volume', volume)


# radio autoplay toggled event, from settings tab
def _on_radio_toggle(toggle: Gtk.CheckButton) -> None:
	settings.set_value('radio', int(toggle.get_active()))


### --- INTERFACE BUILDERS --- ###


# (re)builds search tab interface
def build_search() -> None:
	# clear old results
	for child in Interface.box_results.get_children():
		child.destroy()

	# write new results (don't ask yt every time!)
	for song in Player.search_results:
		# keep length reasonable. this would make the most sense with a monospace
		# font, but works fine either way
		uploader_name = f' ({song["author"]})'
		whole_title = song['title'] + uploader_name
		if len(whole_title) > 70:
			whole_title = (
                f"{song['title'][:67 - len(uploader_name)]}",
                '...{uploader_name}'
            )

		# clickable song title (plays song)
		btn_song = Gtk.Button.new_with_label(whole_title)
		btn_song.set_tooltip_text('Play song')
		btn_song.set_halign(Gtk.Align.FILL)
		btn_song.set_relief(Gtk.ReliefStyle.NONE)
		btn_song.get_children()[0].set_halign(Gtk.Align.START)
		btn_song.connect('clicked', _on_play_queue, [song], 0)

		if not Player.online and not cache.is_song_cached(song['id']):
			btn_song.set_sensitive(False)

		# add to playlist menu
		men_add = Gtk.Menu.new()
		for playlist in playlists.read_playlists().keys():
			itm_playlist = Gtk.MenuItem.new_with_label(playlist)
			itm_playlist.connect(
				'activate',
				_on_add_to_playlist,
				song['id'],
				song['title'],
				song['author'],
				playlist
			)
			men_add.append(itm_playlist)

		men_add.show_all()

		# add to playlist button
		btn_add = Gtk.MenuButton()
		btn_add.set_tooltip_text('Add song to playlist')
		btn_add.add(Gtk.Label.new('+'))
		btn_add.set_popup(men_add)

		# disable add to if no playlists exist
		if len(men_add.get_children()) == 0:
			btn_add.set_sensitive(False)

		# box for single song
		box_result = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
		box_result.set_hexpand(True)
		box_result.set_halign(Gtk.Align.FILL)
		box_result.pack_start(btn_add, False, False, 0)
		box_result.pack_start(btn_song, True, True, 5)
		Interface.box_results.pack_start(box_result, False, False, 2)

	Interface.box_results.show_all()


# (re)builds playlists tab interface
def build_playlists() -> None:
	# for use with Interface.collapsed_playlists. widget:playlist_name
	hideable_boxes = {}

	# clear playlists for reload
	for child in Interface.box_playlists.get_children():
		child.destroy()

	# new playlist name entry
	ent_new = Gtk.Entry.new()
	ent_new.set_placeholder_text('Enter name...')

	# create/import playlist buttons
	btn_new = Gtk.Button.new_with_label('Create')
	btn_new.set_tooltip_text('Create an empty playlist')
	btn_new.connect('clicked', _on_create_playlist, ent_new)
	btn_imp = Gtk.Button.new_with_label('Import...')
	btn_imp.set_tooltip_text('Create a playlist using exported data')
	btn_imp.connect('clicked', _on_import_playlist, ent_new)

	# box for playlist creator
	box_new = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
	box_new.set_valign(Gtk.Align.START)
	box_new.pack_start(ent_new, True, True, 5)
	box_new.pack_start(btn_new, False, False, 5)
	box_new.pack_start(btn_imp, False, False, 5)
	Interface.box_playlists.pack_start(box_new, False, False, 5)

	# box for playlists
	box_lists = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

	# all playlists
	for name, content in playlists.read_playlists().items():
		# expand playlist button
		btn_expand = Gtk.Button.new_with_label(
			'▶' if name in Interface.collapsed_playlists else '▼'
		)
		btn_expand.set_halign(Gtk.Align.START)
		btn_expand.set_relief(Gtk.ReliefStyle.NONE)
		# note that .connect happens further down

		# shuffle-play playlist button
		btn_shuffle = Gtk.Button.new_with_label('⤭')
		btn_shuffle.set_tooltip_text('Shuffle play this playlist')
		btn_shuffle.set_halign(Gtk.Align.START)
		shuffled = content.copy()
		shuffle(shuffled)
		btn_shuffle.connect('clicked', _on_play_queue, shuffled, 0)

		# menu with more playlist actions
		men_more = Gtk.Menu.new()
		itm_delete = Gtk.MenuItem.new_with_label('Delete')
		itm_delete.connect('activate', _on_delete_playlist, name)
		itm_uncache = Gtk.MenuItem.new_with_label('Remove from local storage')
		itm_uncache.connect('activate', _on_uncache_playlist, name)
		itm_share = Gtk.MenuItem.new_with_label('Share...')
		itm_share.connect('activate', _on_share_playlist, name)
		itm_rename = Gtk.MenuItem.new_with_label('Rename...')
		itm_rename.connect('activate', _on_rename_playlist, name)
		men_more.append(itm_rename)
		men_more.append(itm_share)
		if Player.online:
			itm_cache = Gtk.MenuItem.new_with_label('Save to local storage')
			itm_cache.connect('activate', _on_cache_playlist, name)
			men_more.append(itm_cache)
		men_more.append(itm_uncache)
		men_more.append(itm_delete)
		men_more.show_all()

		# button for "more" menu
		btn_more = Gtk.MenuButton()
		btn_more.add(Gtk.Label.new('...'))
		btn_more.set_popup(men_more)

		# playlist name
		lbl_playlist = Gtk.Label.new(name)
		lbl_playlist.set_halign(Gtk.Align.START)

		# box for playlist header
		box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
		box.set_halign(Gtk.Align.START)
		box.set_valign(Gtk.Align.START)
		box.pack_start(btn_expand, False, False, 5)
		box.pack_start(btn_shuffle, False, False, 5)
		box.pack_start(btn_more, False, False, 0)
		box.pack_start(lbl_playlist, False, False, 5)
		box_lists.pack_start(box, False, False, 5)

		# box for all songs in playlist
		box_songs = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
		hideable_boxes[box_songs] = name
		btn_expand.connect('clicked', _on_expand_playlist, box_songs, name)

		# all playlist songs
		for i, song in enumerate(content):
			# keep length reasonable. this would make the most sense with a
			# monospace font, but works fine either way
			whole_title = song['title']
			if len(whole_title) > 70:
				whole_title = song['title'][:67] + '...'

			# clickable song title (plays playlist starting with song)
			btn_song = Gtk.Button.new_with_label(whole_title)
			btn_song.set_tooltip_text('Play song')
			btn_song.set_halign(Gtk.Align.FILL)
			btn_song.set_relief(Gtk.ReliefStyle.NONE)
			btn_song.get_children()[0].set_halign(Gtk.Align.START)
			btn_song.connect('clicked', _on_play_queue, content, i)

			# "more" menu - song actions
			men_more = Gtk.Menu.new()
			if i != 0:
				itm_up = Gtk.MenuItem.new_with_label('Move up')
				itm_up.connect(
                    'activate', _on_swap_in_playlist, i, i - 1, name
                )
				men_more.append(itm_up)
			if i != len(content) - 1:
				itm_down = Gtk.MenuItem.new_with_label('Move down')
				itm_down.connect(
                    'activate', _on_swap_in_playlist, i, i + 1, name
                )
				men_more.append(itm_down)
			itm_rename = Gtk.MenuItem.new_with_label('Rename')
			itm_rename.connect(
                'activate', _on_rename_song, i, song['title'], name
            )
			men_more.append(itm_rename)
			if cache.is_song_cached(song['id']):
				itm_uncache = Gtk.MenuItem.new_with_label(
                    'Remove from local storage'
                )
				itm_uncache.connect('activate', _on_uncache_song, song['id'])
				men_more.append(itm_uncache)
			elif Player.online:
				itm_cache = Gtk.MenuItem.new_with_label(
                    'Save to local storage'
                )
				itm_cache.connect('activate', _on_cache_song, song['id'])
				men_more.append(itm_cache)
			else:
				btn_song.set_sensitive(False)
			itm_remove = Gtk.MenuItem.new_with_label('Remove from playlist')
			itm_remove.connect('activate', _on_remove_from_playlist, i, name)
			men_more.append(itm_remove)
			men_more.show_all()

			# button for "more" menu
			btn_more = Gtk.MenuButton()
			btn_more.add(Gtk.Label.new('...'))
			btn_more.set_popup(men_more)

			# box for song
			box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
			box.set_margin_start(10)
			box.set_hexpand(True)
			box.set_halign(Gtk.Align.FILL)
			box.pack_start(btn_more, False, False, 0)
			box.pack_start(btn_song, True, True, 5)
			box_songs.pack_start(box, False, False, 2)

		box_lists.pack_start(box_songs, False, False, 5)

	# scroller for box for playlists
	scr_lists = Gtk.ScrolledWindow()
	scr_lists.set_vexpand(True)
	scr_lists.add(box_lists)

	Interface.box_playlists.pack_start(scr_lists, True, True, 5)
	Interface.box_playlists.show_all()

	# playlists should stay collapsed
	for widget, name in hideable_boxes.items():
		if name in Interface.collapsed_playlists:
			widget.set_visible(False)


# builds settings tab interface. SHOULD ONLY HAPPEN ONCE ON APP INIT
def build_settings() -> None:
	# volume label
	lbl_volume = Gtk.Label.new('Volume')

	# volume control
	scl_volume = Gtk.Scale.new_with_range(
        Gtk.Orientation.HORIZONTAL, 0, 1, 0.1
    )
	scl_volume.set_draw_value(False)
	scl_volume.connect('value-changed', _on_volume_change)

	try:
		v = float(settings.get_value('volume'))
		scl_volume.set_value(v)
	except ValueError:
		scl_volume.set_value(1)
		settings.set_value('volume', 1)

	# box for volume
	box_volume = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
	box_volume.set_halign(Gtk.Align.FILL)
	box_volume.pack_start(lbl_volume, False, False, 10)
	box_volume.pack_start(scl_volume, True, True, 10)

	# autoplay radio checkbox
	chk_autoplay = Gtk.CheckButton.new_with_label(
        'Autoplay similar songs after queue ends'
    )
	chk_autoplay.connect('toggled', _on_radio_toggle)

	try:
		v = bool(int(settings.get_value('radio')))
		chk_autoplay.set_active(v)
	except ValueError:
		chk_autoplay.set_active(False)
		settings.set_value('radio', 0)

	# box for radio
	box_autoplay = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
	box_autoplay.set_halign(Gtk.Align.FILL)
	box_autoplay.pack_start(chk_autoplay, False, False, 10)

	# box for settings
	box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
	box.pack_start(box_volume, False, False, 5)
	box.pack_start(box_autoplay, False, False, 5)

	Interface.scr_settings.add(box)
	Interface.scr_settings.show_all()


# builds the app's interface. SHOULD ONLY HAPPEN ONCE ON APP INIT
def build(app) -> None:
	logging.basicConfig(level = logging.INFO)

	## player
	# current song title/author
	Interface.lbl_title = Gtk.Label()
	Interface.lbl_title.set_halign(Gtk.Align.START)
	Interface.lbl_title.set_line_wrap(True)
	Interface.lbl_title.set_label('...')

	# song progress thing
	Interface.bar_progress = Gtk.ProgressBar.new()
	Interface.bar_progress.set_show_text(False)
	Interface.bar_progress.set_halign(Gtk.Align.FILL)
	Interface.bar_progress.set_valign(Gtk.Align.CENTER)
	GLib.timeout_add_seconds(1, update_progress)

	# current song info box
	box_info = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
	box_info.set_halign(Gtk.Align.FILL)
	box_info.pack_start(Interface.lbl_title, False, False, 1)
	box_info.pack_start(Interface.bar_progress, True, True, 0)

	# playback control buttons
	Interface.btn_pause = Gtk.Button.new_with_label('▶')
	Interface.btn_pause.set_tooltip_text('Pause/unpause')
	Interface.btn_pause.connect('clicked', _on_toggle_pause)
	Interface.btn_pause.set_relief(Gtk.ReliefStyle.NONE)
	btn_next = Gtk.Button.new_with_label('▶▶❙')
	btn_next.set_tooltip_text('Next song')
	btn_next.connect('clicked', _on_next_song)
	btn_next.set_relief(Gtk.ReliefStyle.NONE)
	btn_prev = Gtk.Button.new_with_label('❙◀◀')
	btn_prev.set_tooltip_text('Previous song')
	btn_prev.connect('clicked', _on_previous_song)
	btn_prev.set_relief(Gtk.ReliefStyle.NONE)
	Interface.tog_loop = Gtk.ToggleButton.new_with_label('⟲')
	Interface.tog_loop.set_tooltip_text('Loop this song')
	Interface.tog_loop.set_relief(Gtk.ReliefStyle.NONE)

	# box for playback controls
	box_controls = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
	box_controls.set_valign(Gtk.Align.CENTER)
	box_controls.set_halign(Gtk.Align.START)
	box_controls.pack_start(btn_prev, True, True, 0)
	box_controls.pack_start(Interface.btn_pause, True, True, 5)
	box_controls.pack_start(btn_next, True, True, 0)
	box_controls.pack_start(Interface.tog_loop, True, True, 5)

	# metabox for player
	box_player = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
	box_player.pack_start(box_controls, False, False, 0)
	box_player.pack_start(box_info, True, True, 10)

	## search tab
	# search entry
	ent_search = Gtk.SearchEntry()
	ent_search.connect('activate', _on_search)

	# results box
	Interface.box_results = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
	scr_results = Gtk.ScrolledWindow()
	scr_results.add(Interface.box_results)

	# search tab
	box_search = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
	box_search.set_border_width(5)
	box_search.pack_start(ent_search, False, False, 0)
	box_search.pack_start(scr_results, True, True, 5)

	## playlists tab
	# playlists tab
	Interface.box_playlists = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

	## settings tab
	# settings tab
	Interface.scr_settings = Gtk.ScrolledWindow()
	Interface.scr_settings.set_vexpand(True)

	## tabbed notebook
	ntb_main = Gtk.Notebook()
	ntb_main.set_margin_bottom(5)
	ntb_main.append_page(Interface.box_playlists, Gtk.Label.new('Playlists'))
	ntb_main.append_page(box_search, Gtk.Label.new('Search'))
	ntb_main.append_page(Interface.scr_settings, Gtk.Label.new('Settings'))
	ntb_main.connect('switch-page', _on_switch_page)

	# status label
	Interface.lbl_status = Gtk.Label.new('')
	Interface.lbl_status.set_halign(Gtk.Align.START)

	# status progress bar
	Interface.bar_status = Gtk.ProgressBar.new()
	Interface.bar_status.set_show_text(False)
	Interface.bar_status.set_halign(Gtk.Align.FILL)
	Interface.bar_status.set_valign(Gtk.Align.CENTER)

	# online mode toggle
	tog_online = Gtk.ToggleButton.new_with_label('...')
	tog_online.set_halign(Gtk.Align.END)
	tog_online.set_tooltip_text('Switch between online and offline mode')
	tog_online.connect('toggled', _on_toggle_online)
	if Player.online:
		tog_online.set_active(True)
		tog_online.set_label('Online')
	else:
		tog_online.set_active(False)
		tog_online.set_label('Offline')

	## status box
	box_status = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
	box_status.set_halign(Gtk.Align.FILL)
	box_status.pack_start(Interface.lbl_status, True, True, 5)
	box_status.pack_start(Interface.bar_status, True, True, 10)
	box_status.pack_start(tog_online, False, False, 5)

	## toplevels
	box_main = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
	box_main.pack_start(box_status, False, False, 0)
	box_main.pack_start(ntb_main, True, True, 0)
	box_main.pack_start(box_player, False, False, 0)
	box_main.set_border_width(5)

	# app window
	Interface.window = Gtk.ApplicationWindow(application=app, title='Myuzi')
	Interface.window.set_default_size(600, 400)
	Interface.window.add(box_main)
	Interface.window.show_all()
	Interface.bar_status.set_visible(False)

	# settings must be built so they can be accessed by the player. playlists
	# must be built last so their tab ends up as the default
	build_settings()
	build_playlists()

