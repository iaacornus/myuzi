import base64
from pathlib import Path
from os import makedirs
from json import loads, dumps
from json.decoder import JSONDecodeError
from binascii import Error as BinasciiError

# playlists = {
#	'my playlist': [
#		{'id': 'ASvGDFQwe', 'name': 'Cool song', 'author': 'Steve'}
#	]
# }


### --- PLAYLIST FUNCTIONS --- ###


def add_playlist(name: str) -> bool:
	new_lists = read_playlists()
	if name not in new_lists:
		new_lists[name] = []
		write_playlists(new_lists)
		return True

	return False


def rename_playlist(name: str, new_name: str) -> bool:
	new_lists = read_playlists()
	if new_name not in new_lists:
		new_lists[new_name] = new_lists.pop(name)
		write_playlists(new_lists)
		return True

	return False


# see export_playlist below
def import_playlist(name: str, data: str) -> tuple[bool, str]:
	new_lists = read_playlists()
	if name not in new_lists:
		try:
			new_lists[name] = loads(
				base64.urlsafe_b64decode(data.encode()).decode()
			)
			write_playlists(new_lists)
			return True, ''
		except (JSONDecodeError, BinasciiError):
			return False, 'Invalid playlist data.'

	return False, 'Playlist exists.'


# returns a b64-string representation of a playlist's contents
def export_playlist(name: str) -> str:
	return base64.urlsafe_b64encode(
		dumps(read_playlists()[name]).encode()
	).decode()


def remove_playlist(name: str) -> None:
	new_lists = read_playlists()
	new_lists.pop(name)
	write_playlists(new_lists)


### --- SONG FUNCTIONS --- ###


def add_song(id_: str, title: str, author: str, playlist: str) -> None:
	new_lists = read_playlists()
	new_lists[playlist].append({'title': title, 'author': author, 'id': id_})
	write_playlists(new_lists)


def rename_song(index: int, playlist: str, new_name: str) -> None:
	new_lists = read_playlists()
	new_lists[playlist][index]['title'] = new_name
	write_playlists(new_lists)


def swap_songs(i: int, j: int, playlist: str) -> None:
	lists = read_playlists()
	lists[playlist][i], lists[playlist][j] = lists[playlist][j], lists[playlist][i]
	write_playlists(lists)


def remove_song(index: int, playlist: str) -> None:
	new_lists = read_playlists()
	new_lists[playlist].pop(index)
	write_playlists(new_lists)


### --- UTILITY FUNCTIONS --- ###


def write_playlists(playlists: dict) -> None:
	dir_path = Path.home() / Path('.config/myuzi/')
	lists_path = dir_path / Path('playlists.json')

	try:
		with open(str(lists_path), 'w', encoding='utf-8') as lists_file:
			lists_file.write(dumps(playlists))
	except FileNotFoundError:
		makedirs(str(dir_path))
		write_playlists(playlists)


def read_playlists() -> dict:
	lists_path = str(
		Path.home() / Path('.config/myuzi/playlists.json')
	)

	try:
		with open(lists_path, 'r', encoding='utf-8') as lists_file:
			return loads(lists_file.read())
	except OSError:
		return {}

