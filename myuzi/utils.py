from json import loads
from subprocess import Popen, PIPE
from random import shuffle
from pathlib import Path
from os.path import exists

from requests import head, get
from requests.exceptions import ConnectionError
from bs4 import BeautifulSoup

from typing import Any
### --- MISC FUNCTIONS --- ###


def copy_to_clipboard(data: str) -> None:
	Popen(
        f'echo "{data}" | xclip -selection clipboard', shell = True
    )


### --- NETWORK FUNCTIONS --- ###


def is_yt_available() -> bool:
	try:
		return head('https://www.youtube.com').status_code == 200
	except ConnectionError:
		return False


# returns song uri given yt video id
def get_song_uri(video_id: str) -> str:
	local_path = str(
        Path.home() / Path('.config/myuzi/cache/' + video_id)
    )
	if exists(local_path):
		return f'file://{local_path}'

	# --no-cache-dir might help with 403 errors
	out, _ = Popen(
		(
            'yt-dlp -g -x --no-cache-dir  '
            f'https://www.youtube.com/watch?v={video_id}'
        ),
		shell=True,
		stdout=PIPE
	).communicate()

	return out.decode().split('\n')[0]


def is_video_music(video_id: str) -> bool | None:
	# not using yt's api because limits
	text = get(f'https://www.youtube.com/watch?v={video_id}').text
	soup = BeautifulSoup(text, 'html.parser')

	# extract video data from json in a js variable
	for script in soup.find_all('script'):
		text = script.text.strip()
		if text.startswith("var ytInitialData ="):
			return 'yt_music_channel' in text


def get_similar_song(
        video_id: str, ignore: list = None
    ) -> dict[str, Any] | None:
	ignore = [] if ignore == None else ignore

	# not using yt's api because limits
	text = get(f'https://www.youtube.com/watch?v={video_id}').text
	soup = BeautifulSoup(text, 'html.parser')

	# extract video data from json in a js variable
	data = {}
	for script in soup.find_all('script'):
		text = script.text.strip()
		keystring = 'var ytInitialData ='
		if text.startswith(keystring):
			data = loads(text.replace(keystring, '').strip(';'))
			break

	vids = (
        data['contents']
        ['twoColumnWatchNextResults']
        ['secondaryResults']
        ['secondaryResults']
        ['results']
    )
	shuffle(vids)

	for video in vids:
		# videos only
		if 'compactVideoRenderer' not in video:
			continue

		metadata = video['compactVideoRenderer']

		# skip ids on ignore list
		if metadata['videoId'] in ignore:
			continue

		# skip non-music videos
		if not is_video_music(metadata['videoId']):
			continue

		# ignore long videos
		length = metadata['lengthText']['simpleText'].split(':')
		if len(length) > 2:
			continue # more than 1 hour
		if int(length[0]) > 9:
			continue # more than 9 minutes

		return {
			'title': metadata['title']['simpleText'],
			'author': metadata['longBylineText']['runs'][0]['text'],
			'id': metadata['videoId']
		}

	return None


# searches yt and returns parsed results
def find_songs(query: str) -> dict[str, Any]:
	result = []

	# not using yt's api because limits
	text = get('https://www.youtube.com/results?search_query={query}').text
	soup = BeautifulSoup(text, 'html.parser')

	# extract video data from json in a js variable
	data = {}
	for script in soup.find_all('script'):
		text = script.text.strip()
		keystring = 'var ytInitialData ='
		if text.startswith(keystring):
			data = loads(text.replace(keystring, '').strip(';'))
			break

	# avert your eyes
	vids = (
        data['contents']
        ['twoColumnSearchResultsRenderer']
        ['primaryContents']
        ['sectionListRenderer']
        ['contents'][0]
        ['itemSectionRenderer']
        ['contents']
    )
	for video in vids:
		# ignore channels, playlists
		if not 'videoRenderer' in video:
			continue

		metadata = video['videoRenderer']

		# ignore livestreams
		if 'badges' in metadata:
			is_live = False
			for badge in metadata['badges']:
				if (
                    badge['metadataBadgeRenderer']['style']
                    == 'BADGE_STYLE_TYPE_LIVE_NOW'
                ):
					is_live = True
					break

			if is_live:
				continue

		# check if official artist channel or verified channel
		verified = False
		official = False
		if 'ownerBadges' in metadata:
			for badge in metadata['ownerBadges']:
				badge_style = badge['metadataBadgeRenderer']['style']
				if badge_style == 'BADGE_STYLE_TYPE_VERIFIED_ARTIST':
					official = True
				elif badge_style == 'BADGE_STYLE_TYPE_VERIFIED':
					verified = True

		result.append({
			'title': metadata['title']['runs'][0]['text'],
			'author': metadata['ownerText']['runs'][0]['text'],
			'id': metadata['videoId'],
			'official': official,
			'verified': verified
		})

	# improved sort
	def sort_key(song: dict) -> int:
		# high bias for verified/official channels
		score = 5 * song['verified'] + 10 * song['official']

		# one point per matching word in query
		for word in query.lower().split(' '):
			if word in song['title'].lower():
				score += 1
			if word in song['author'].lower():
				score += 1

		# of course lowest numbers go first in sorting
		return -score

	result.sort(key=sort_key)
	return result

